import React, { Component } from "react";

class Details extends Component {
  state = {};

  render() {
    const device = this.props.device;
    return (
      <div className="container rounded border border-primary">
        <h4>Name: {device.name}</h4>
        <div>Description: {device.description}</div>
        <div>Location: {device.location}</div>
        <div>Last result: {device.lastResult}</div>
      </div>
    );
  }
}

export default Details;
