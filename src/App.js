import React, { Component } from "react";
import "./App.css";
import Search from "./components/search";
import Devices from "./components/devices";
import Details from "./components/details";

class App extends Component {
  state = {};

  constructor(props) {
    super(props);

    this.state = {
      isLoaded: false,
      devices: [],
      display: [],
      details: []
    };
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData() {
    fetch(
      "https://gist.githubusercontent.com/Tehnix/c9d53939b2ed600b321fed8e1898f3a7/raw/dfe7adaf6f73e320ae4ed42761d6b50cf25eb569/devices.json"
    )
      .then(res => res.json())
      .then(json => {
        this.setState({
          isLoaded: true,
          devices: json.devices,
          display: json.devices
        });
      });
  }

  handleDetails = device => {
    this.setState({ details: device });
  };

  handleSearch = display => {
    this.setState({ display });
  };

  render() {
    if (!this.state.isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <div className="col">
          <Search
            devices={this.state.devices}
            onSearch={this.handleSearch}
            className="navbar navbar-light bg-light"
          />
          <div className="row">
            <div className="col">
              <Devices
                devices={this.state.display}
                onDetails={this.handleDetails}
              />
            </div>

            <div className="col">
              <Details device={this.state.details} />
            </div>
          </div>
        </div>
      );
    }
  }
}

export default App;
