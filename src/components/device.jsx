import React, { Component } from "react";

class Device extends Component {
  state = {};

  render() {
    return (
      <button
        className="row btn btn-sm btn-block btn-primary"
        onClick={() => this.props.onDetails(this.props.device)}
      >
        <div className="col">Name: {this.props.device.name} </div>
        <div className="col">Location: {this.props.device.location} </div>
      </button>
    );
  }
}

export default Device;
