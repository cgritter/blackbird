import React, { Component } from "react";
import Device from "./device";

class Devices extends Component {
  render() {
    return (
      <div className="container">
        <div className="col">
          {this.props.devices.map(device => (
            <Device
              key={device.name}
              device={device}
              onDetails={this.props.onDetails}
            />
          ))}
        </div>
      </div>
    );
  }
}

export default Devices;
