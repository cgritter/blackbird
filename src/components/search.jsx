import React, { Component } from "react";

class Search extends Component {
  mySearch = () => {
    const input = document.getElementById("myInput");
    const filter = input.value.toUpperCase();
    const devices = this.props.devices;

    let display = [];

    for (let i = 0; i < devices.length; i++) {
      if (devices[i].name) {
        if (
          devices[i].name.toUpperCase().indexOf(filter) > -1 ||
          devices[i].description.toUpperCase().indexOf(filter) > -1
        ) {
          display[i] = devices[i];
        }
      }
    }
    this.props.onSearch(display);
  };

  render() {
    return (
      <div className="navbar navbar-light bg-light">
        <h1 className="navbar-brand">Search</h1>
        <input
          type="text"
          id="myInput"
          onKeyUp={this.mySearch}
          placeholder="Search for devices.."
          className="form-control"
          aria-label="Sizing example input"
          aria-describedby="inputGroup-sizing-sm"
        />
      </div>
    );
  }
}

export default Search;
